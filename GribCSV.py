import xarray as xr
import matplotlib.pyplot as plt

grib_file_path = r"<your path>" #path to grib file

ds = xr.open_dataset(grib_file_path, engine='cfgrib') #to open grib files

solar_radiation_data = ds['ASWDIR_S'] #using al variable for this grib file.
#variable changes according to the data variable in the grib file
#to check the data variable use following lines
print("Data Type:", solar_radiation_data.dtype)
print("Unique Values:", solar_radiation_data.values)
print(solar_radiation_data)

#for converting grib data in csv files
csv_file_path = r'path to save file after analysis'
df = solar_radiation_data.to_dataframe(name='SolarRadiation').reset_index()
df.to_csv(csv_file_path, index=False)

#to plot the required graph of radiation
solar_radiation_data.plot()
plt.title('Solar Radiation Data')
plt.xlabel('time')
plt.ylabel('ASWDIR_S')
plt.show()