import xarray as xr
import matplotlib.pyplot as plt

grib_file_path = r'grib file path'
ds = xr.open_dataset(grib_file_path, engine='cfgrib')  #to open grib files

radiation_data = ds['al'] #using al variable for this grib file.
print(radiation_data)

#to convert the grib data generated in json form
json_file_path = r'file path to save json file'
df_json = radiation_data.to_dataframe(name='Radiation').reset_index()
df_json.to_json(json_file_path, orient='records', lines=True)

#to plot the required graph of radiation
radiation_data.plot()
plt.title('Radiation Data')
plt.xlabel('Longitude')
plt.ylabel('Latitude')
plt.show()
