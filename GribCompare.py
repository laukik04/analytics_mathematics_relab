import xarray as xr
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

class GribFileHandler:
    def __init__(self, file_path):
        self.file_path = file_path
        self.ds = xr.open_dataset(file_path, engine='cfgrib', chunks=None)

    def get_radiation_data(self, lat_slice=slice(None), lon_slice=slice(None)):
        time = self.ds['time'].values
        radiation = self.ds['ASWDIR_S'].sel(latitude=lat_slice, longitude=lon_slice).values

        #to ensure radiation has the expected shape
        if radiation.ndim == 3:
            return np.arange(10), radiation[:, :10, :10]
        else:
            print(f"Skipping {self.file_path} due to shape mismatch:")
            print(f"Radiation shape: {radiation.shape}")
            return None

class CSVHandler:
    def __init__(self):
        self.dfs = []

    def add_data(self, data, file_name):
        columns = [f"Step_{i+1}" for i in range(min(50, data.shape[0]))]
        flattened_data = data[:, :min(10, data.shape[1])].reshape(-1, order='F')
        reshaped_data = flattened_data.reshape(-1, len(columns), order='F')
        df = pd.DataFrame(reshaped_data, columns=columns)
        self.dfs.append((file_name, df))

    def save_to_csv(self):
        for file_name, df in self.dfs:
            df.to_csv(f'{file_name}_radiation_data.csv', index=False)


class GribAnalyzer:
    def __init__(self, grib_file_paths):
        self.grib_file_paths = grib_file_paths
        self.grib_file_handlers = [GribFileHandler(file_path) for file_path in grib_file_paths]
        self.csv_handler = CSVHandler()

    def plot_radiation_data(self, lat_slice=slice(None), lon_slice=slice(None)):
        fig, axs = plt.subplots(nrows=4, ncols=2, figsize=(10, 10))

        for i, grib_handler in enumerate(self.grib_file_handlers):
            row = i // 2
            col = i % 2
            self._plot_single_radiation(axs[row, col], grib_handler, lat_slice, lon_slice)

        fig.suptitle('Radiation data for multiple GRIB2 files', fontsize=10)
        plt.tight_layout(rect=[0, 0.03, 1, 0.95])  #to adjust layout to include suptitle
        plt.show()

        self.csv_handler.save_to_csv()

    def _plot_single_radiation(self, ax, grib_handler, lat_slice, lon_slice):
        data = grib_handler.get_radiation_data(lat_slice, lon_slice)
        if data is not None:
            x_values, radiation = data
            min_len = min(len(x_values), radiation.shape[1])
            for i in range(min(50, radiation.shape[0])):
                ax.plot(x_values[:min_len], radiation[i][:min_len])

            ax.legend()
            ax.set_xlabel('time')
            ax.set_ylabel('Direct Radiation')
            file_name = grib_handler.file_path.split("\\")[-1]
            ax.set_title(f'Radiation Data for {file_name}')
            ax.set_ylim(0, 4)  # Set y-axis range from 0 to 4

            # Add data to CSV handler
            self.csv_handler.add_data(radiation, file_name)

#path to the grib files stored in device
grib_file_paths = [
    r"file path",
    r"file path",
    r"file path",
    r"file path",
    r"file path",
    r"file path",
    r"file path",
    r"file path"
]

#creating an instance of GribAnalyzer class
grib_analyzer = GribAnalyzer(grib_file_paths)

#calling the method to plot radiation data for all files with desired coordinates(nearest coordinates to Emden City) and save to CSV
grib_analyzer.plot_radiation_data(lat_slice=slice(53, 54), lon_slice=slice(8, 9))
